var gulp=require("gulp");
var watch=require("gulp-watch");
var postcss=require("gulp-postcss");
var autoprefixer=require("autoprefixer");
var cssvars=require("postcss-simple-vars");
var imports=require("postcss-import");
var nesteds=require("postcss-nested");
var browsersync=require("browser-sync").create();

gulp.task('default',function(){
    console.log("hey: gulp installed and running the default task:");
});

gulp.task('html',function(){
    console.log("some task related to html is done here");
});

gulp.task('css',function(){
    console.log("inside css");
    return gulp.src("./app/assets/styles/style.css").pipe(postcss([imports,nesteds,cssvars,autoprefixer])).pipe(gulp.dest("./app/temp/styles"));
});
gulp.task('cssInject',function(){
    return gulp.src("./app/assets/styles/style.css").pipe(browsersync.stream());
});
gulp.task('watch',function(){
    browsersync.init({
        notify:false,
        server:{
            baseDir:"app"
        }
    });
    watch("./app/index.html",function(){
        browsersync.reload();
    });
    watch("./app/index.html",gulp.series("html"));
    watch("./app/index.html",gulp.series("css"));
    watch("./app/temp/styles/*.css",function(){
        browsersync.reload();
    });
    watch("./app/assets/styles/**/*.css",gulp.series("css"));
});


























































